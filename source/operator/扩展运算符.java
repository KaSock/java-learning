public class Demo {
    public static void main(String[] args) {
        //扩展赋值运算符
        int a =10;
        int b =20;

        a+=b;  //a= a+b
        a-=b;  //a= a-b  line8:a=30
        System.out.println(a); //10

        System.out.println("---------------------------------------");

        //字符串连接符  +  ,String
        System.out.println(a+b);
        System.out.println(""+a+b); //先有字符串，后续强行转换为String类型
        System.out.println(a+b+"草"); //先运算，再输出字符串
    }
}