public class Demo {
    public static void main(String[] args) {
        // 二元运算符
        //Ctrl+D 复制当前行到下一行
        int a = 1;
        int b = 4;
        int c = 7;
        int d = 10;

        System.out.println(a+b);
        System.out.println(a-c);
        System.out.println(a*d);
        System.out.println(a/(double)b);

        System.out.println("================================================");

        long e = 543215345345L;
        int f =123;
        short g =10;
        byte h = 8;

        System.out.println(e+f+g+h); //Long
        System.out.println(f+g+h);
        System.out.println(g+h);
        //有Long类型结果默认Long类型，有double默认double类型

        System.out.println("================================================");

        //关系运算符返回结果 True False 布尔值
        //if

        int j =10;
        int k =20;
        int l =21;

        System.out.println(j>k);
        System.out.println(j<k);
        System.out.println(j==k);
        System.out.println(j!=k);

        //取余，模运算
        System.out.println(l%j);   //l除以j 21/10=2......1

    }
}