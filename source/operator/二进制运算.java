public class Demo {
    public static void main(String[] args) {
        /*
        位运算 涉及二进制 & | ^
        A = 0011 1100
        B = 0000 1101
        ---------------------------------------
        A&B = 0000 1100  有一个0则为0                    -与运算
        A|B = 0011 1101  有一个1则为1                    -或运算
        A^B = 0011 0001  两个位相同则为0 不同则为1         -异或运算
        ~B  = 1111 0010  把B全部取反                     -取反
        ~A  = 1100 0011  把A全部取反                     -取反

        2*8 = 16   2*2*2*2
        效率极高↓
        <<  *  左移
        >>  /  右移

        0000 0000     0
        0000 0001     1
        0000 0010     2
        0000 0100     4
        0000 0011     3
        0000 1000     8
        0001 0000     16
         */

        System.out.println(2<<3);
    }
}