public class Demo {
    public static void main(String[] args) {
        printMax(34,215,66.2,3356.3,4521.2);
        printMax(new double[]{3,4,5});
    }


    public static void printMax(double... numbers) {
        if (numbers.length == 0){
            System.out.println("GG");
            return;
        }

        double result = numbers[0];

        for (int i = 1; i < numbers.length; i++) {
            if (numbers[i]>result){
                result = numbers[i];
            }
        }
        System.out.println("最大数是"+ result);
    }

}