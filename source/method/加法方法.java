public class Demo {
    //main方法
    public static void main(String[] args) {

        //实际参数，实际调用传递给他的参数
        int sum = add(1, 2);
        System.out.println(sum);

        System.out.println("----------");

        test();
    }

    //加法
    //形式参数，用来定义的作用的
    public static int add(int a,int b){
        return a+b;
    }

    //输出
    public static void test(){
        for (int i = 5; i <= 1000; i++){
            if (i%5==0){
                System.out.print(i+"\t");
            }
            if (i%(5*3)==0) {//每行
                System.out.println();
                //System.out.print("\n");
            }
        }
    }
}