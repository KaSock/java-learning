import java.util.Scanner;

public class Demo {
    public static void main(String[] args) {
        //创建一个扫描器对象，用于接收键盘数据
        Scanner scanner = new Scanner(System.in);


        //判断用户有没有输入字符串
        if (scanner.hasNext()){
            //接收用户的输入
            String str = scanner.next(); //程序会等待用户输入完毕
            System.out.println("输入的内容为："+str);
        }
        scanner.close();
        //凡是IO流，如果不关闭会一直占用资源，养成用完就关的好习惯
    }
}