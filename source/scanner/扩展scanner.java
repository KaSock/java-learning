import java.util.Scanner;

public class Demo {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        //从键盘接收数据
        int i =0;
        float f =0.0f;

        System.out.println("检查整数：");

        //如果...那么...
        if (scanner.hasNextInt()){
            i = scanner.nextInt();
            System.out.println("整数数据"+i);
        }else {
            System.out.println("输入的不是整数！");
        }

        System.out.println("检查小数：");

        if (scanner.hasNextFloat()){
            f = scanner.nextFloat();
            System.out.println("小数数据"+f);
        }else {
            System.out.println("输入的不是小数！");
        }

        scanner.close();
    }
}