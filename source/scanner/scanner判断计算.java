import java.util.Scanner;

public class Demo {
    public static void main(String[] args) {
        //输入多个数字，并求和与平均数，每输入一个数字回车确认，通过输入非数字来结束输入并输出执行结果

        Scanner scanner = new Scanner(System.in);

        //和
        double sum=0;
        //计算输入了多少个数字
        int m=0;

        System.out.println("输入数据：");

        //通过循环判断是否还有输入，并在里面对每一次进行求和与统计
        while (scanner.hasNextDouble()){   //非数字结束循环
            double x = scanner.nextDouble();
            m=m+1;//m++
            sum = sum + x;
            System.out.println("这是第"+m+"个数据，当前和为"+sum+"当前平均值为"+(sum/m));

        }
        System.out.println(m+"个数和为："+sum);
        System.out.println(m+"个数平均值为："+(sum/m));

        scanner.close();
    }
}