public class DoWhileDemo02 {
    public static void main(String[] args) {
        int a = 0;
        while (a<0){
            System.out.println(a);
            a++;
        }              //无法输出

        System.out.println("--------------");

        do {
            System.out.println(a);
            a++;            //先输出a=0 再去判断while条件
        }while (a<0);
    }
}