import java.util.Scanner;

public class IfDemo02 {
    public static void main(String[] args) {
        //例 考试分数大于60及格，小于60不及格

        Scanner scanner = new Scanner(System.in);

        System.out.println("输入你的成绩");
        int score = scanner.nextInt();

        if (score>=60){
            System.out.println("及格");
        }else{
            System.out.println("不及格");
        }


        scanner.close();
    }
}