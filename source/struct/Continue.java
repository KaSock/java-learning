public class ContinueDemo {
    public static void main(String[] args) {
        int i = 0;
        while (i<100){
            i++;
            if (i%10==0){
                System.out.println();
                continue;
            }
            System.out.print(i+" ");
        }
        //continue 语句用于循环语句体中，用于停止某次循环过程，即跳过循环体中尚未执行的语句，接着进行下一次循环判定
    }
}