public class ForDemo04 {
    public static void main(String[] args) {
        
        /*
        1.先打印第一列
        2.然后把固定的1用循环包起来
        3.去重复项 i<=g
        4.调整样式
         */


        for (int g = 1; g <= 9; g++) {
            for (int i = 1; i <= g; i++) {
                System.out.print(g+"*"+i+"="+(g*i) + "\t");
            }
            System.out.println();
        }
    }
}