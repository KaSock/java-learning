public class SwitchDemo02 {
    public static void main(String[] args) {
        String name = "kai";
        //jdk7新特性 switch表达式结果可以是字符串
        //字符的本质还是数字

        //反编译  java---class（字节码文件）---反编译（IDEA）
        //Ctrl+Alt+Shift+S 打开项目结构
        switch (name){
            case "Kai":
                System.out.println("Kai");
                break;
            case "kai":
                System.out.println("kai");
                break;
            default:
                System.out.println("弄啥嘞？");
        }

    }
}