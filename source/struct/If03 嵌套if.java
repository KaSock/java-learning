import java.util.Scanner;

public class IfDemo03 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        /*
        if 语句里至少有1个 else 语句， else 语句在所有 else if 语句之后。
        if 语句可以有若干个 else if 语句，他们必须在 else 语句之前。
        一旦其中一个 else if 语句检测为 true ，其他的 else if 以及 else 语句都将跳过执行。
        有 else if 语句就必须有 else 语句。
         */

        System.out.println("输入你的成绩");
        int score = scanner.nextInt();

        if (score==100){
            System.out.println("恭喜满分");
        }else if (score<100 && score>=90){
            System.out.println("A级");
        }else if (score<90 && score>=80){
            System.out.println("B级");
        }else if (score<80 && score>=70){
            System.out.println("C级");
        }else if (score<70 && score>=60){
            System.out.println("D级");
        }else if (score<60 && score>=0){
            System.out.println("不及格");
        }
        else {
            System.out.println("成绩不合法");
        }

        System.out.println("-------------------");

        //嵌套if
        String today = "周一";
        String weather = "晴天";
        if (today.equals("周末")){
            if (weather.equals("晴天")){
                System.out.println("出去玩");
            }else{
                System.out.println("家里玩");
            }
        }else{
            System.out.println("去上班");
        }



        scanner.close();
    }
}