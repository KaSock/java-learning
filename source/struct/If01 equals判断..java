import java.util.Scanner;

public class IfDemo01 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("输入：");
        String s = scanner.nextLine();

        //equals:判断字符串是否相等
        if (s.equals("Hello")){
            System.out.println("你好");
        }else
            System.out.println("End");


        scanner.close();
    }
}