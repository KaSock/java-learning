public class BreakDemo {
    public static void main(String[] args) {
        int i =0;
        //break 在任何循环语句的主体部分，均可用break控制循环的流程，break用于强制退出循环，不执行循环中剩下的语句
        while (i<100){
            i++;
            System.out.println(i);
            if (i==30){
                break;
            }
        }
    }
}