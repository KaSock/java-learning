public class Demo {
    public static void main(String[] args) {
        //操作大数值 注意溢出问题
        //jdk新特性 _不会输出
        int money=10_0000_0000;
        System.out.println(money);

        int years =20;
        int total = money*years;
        System.out.println(total);//溢出（-1474836480）
        long total2=money*years;//默认int，先计算再转换（-1474836480）

        long total3= money*((long)years);//先把一个数转换为long类型

        System.out.println(total2);
        System.out.println(total3);

        // L  l  尽量使用大写L
    }
}