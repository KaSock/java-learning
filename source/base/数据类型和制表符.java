public class Demo {
    public static void main(String[] args) {
        //整数
        int i = 10 ;    //十进制
        int i2=010;     //八进制
        int i3=0x10;    //十六进制

        System.out.println(i);
        System.out.println(i2);
        System.out.println(i3);
        System.out.println("=================================================================");

        //浮点数 银行业务  用BigDecimal
        //BigDecimal  数字工具类
        //float     有限    离散   舍入误差     大约   接近但不等于
        //double
        //避免使用浮点数进行比较

        float f = 0.1f;  //0.1
        double d = 1.0/10;  //0.1
        // == 判断是否相等
        System.out.println(f==d); //false
        System.out.println(f);
        System.out.println(d);

        float f1= 11451234342344f;
        float f2= f1 + 1;
        System.out.println(f1==f2);//true 出现舍入误差

        float f3 =1919810f;
        float f4 =f3+1;
        System.out.println(f3==f4);//false 未出现误差
        System.out.println("=================================================================");

        //字符拓展
        char c1='a';
        char c2='中';

        System.out.println((int)c1);//强制转换

        System.out.println((int)c2);//强制转换

        //所有字符本质还是数字
        //编码 Unicode   2字节  0 - 65536  Excel 最高 2^16 = 65536

        //U0000 UFFFF
        char c3 = '\u0061';
        System.out.println(c3);//a

        System.out.println("=================================================================");

        //转义字符
        //  \t 制表符  \u0009
        //  \n 换行    \u000a
        //  \b 退格    \u0008
        //  \r 回车    \u000d

        System.out.println("hello\tworld");
        System.out.println("hello\nworld");
        System.out.println("hello\bworld");//hellworld
        System.out.println("helloworl\nd");//d   回车(CR) ，将当前位置移到本行开头

        System.out.println("=================================================================");


        String sa = new String("hello world");
        String sb = new String("hello world");
        System.out.println(sa==sb);

        String sc = "hello world";
        String sd = "hello world";
        System.out.println(sc==sd);
        //对象  从内存分析

        //布尔值扩展
        boolean flag=true;

        if(flag==true){} //新手
        if(flag){}   //简化
        //Less is more 代码要精简易读


    }
}