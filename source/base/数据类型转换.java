public class Demo {
    //类型转换
    //低------------------------------------------------高
    // byte - short - char - int - long - float - double  小数优先级一定大于整数

    public static void main(String[] args) {
        int i=128;
        byte b =(byte)i;//最大值127 内存溢出

        //强制转换  (类型)变量名  高--低
        System.out.println(i);
        System.out.println(b);//-128

        //自动转换  低--高
        double c =i;
        System.out.println(c);

        /*
        注意点：
        1.不能对布尔值进行转换
        2.不能把对象类型转相换为不干的类型
        3.在高容量转换到低容量，使用强制转换
        4.转换的时候可能存在内存溢出或精度问题
         */

        System.out.println("=================================================================");
        System.out.println((int)16.5); //16
        System.out.println((int)-45.59f); //-45

        System.out.println("=================================================================");
        char d='a';
        int f =d+3;
        System.out.println(f);
        System.out.println((char) f);

    }

}