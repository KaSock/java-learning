public class ShunXuDemo {
    public static void main(String[] args) {
        /*
        语句与语句之间，框与框之间是按从上往下的顺序进行的，它是由若干个依次执行
        的处理步骤组成的，它是任何一个算法都离不开的一钟基本算法结构。
         */
        System.out.println("1");
        System.out.println("2");
        System.out.println("3");
    }
}