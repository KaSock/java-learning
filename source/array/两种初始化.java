public class Demo {
    public static void main(String[] args) {
        //静态初始化数组:创建+赋值
        int[] a = {1,2,3,4,5};

        Man[] mans = {new Man(),new Man()};

        System.out.println(a[0]);

        for (int i = 0; i < a.length; i++) {
            System.out.println(a[i]);
        }





        //动态初始化:包含默认初始化
        int[] b = new int[10];
        b[0] = 10;

        System.out.println(b[0]);
        System.out.println(b[1]);//默认初始化

    }
}