public class Demo {
    public static void main(String[] args) {
        int[] arrays = {1,2,3,4,5};

//        //JDK1.5  没有下标
//        for (int array : arrays) {
//            System.out.println(array);
//        }

        //printArray(arrays);

        int[] reverse = reverse(arrays);
        printArray(reverse);
    }

    //打印数组元素
    public static void printArray(int[] arrays){
        for (int i = 0; i < arrays.length; i++) {
            System.out.print(arrays[i]+" ");
        }

        }

    //反转数组
    public static int[] reverse(int[] array){
        int[] result = new int[array.length];

        //反转操作
        for (int i = 0,j= result.length-1; i < array.length; i++,j--) {
                result[j] = array[i];
        }

        return result;
    }