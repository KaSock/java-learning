import java.util.Arrays;

//冒泡排序
    /*
      1.比较数组中，两个相邻元素，如果第一个数比第二个数大，则交换位置
      2.每次比较都会产生一个最大或最小的数字
      3.下一轮则少一次排序
      4.依次循环，直到结束
     */
public class Demo {
    public static void main(String[] args) {

        int[]b = {1,654,7748,2,54,32,657,44};

        int[] sort = sort(b);  //返回新数组
        System.out.println(Arrays.toString(b));
    }

    static  int[] sort(int[] array){
        //临时变量
        int a =0;

        //外层循环，确定循环次数
        for (int i = 0; i < array.length - 1; i++) {
            boolean flag = false;  //减少没意义的比较
            //内存循环判断两数大小 交换位置
            for (int j = 0; j < array.length - 1 - i; j++) {
                if (array[j+1]<array[j]){
                    a = array[j];
                    array[j] = array[j+1];
                    array[j+1] = a;
                    flag = true;
                }
            }
            if (flag==false){
                break;
            }
        }
        return array;
    }
}