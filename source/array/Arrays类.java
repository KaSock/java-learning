import java.util.Arrays;

public class Demo {
    public static void main(String[] args)
    {

        int[] a = {1,3,5,777,8676,34324,2325,5657,78};

        System.out.println(Arrays.toString(a));       //一键打印数组
        System.out.println("------------------");
        printArray(a);
        System.out.println("\n==================");
        Arrays.sort(a);                               //对数组进行排序
        System.out.println(Arrays.toString(a));

        System.out.println("==================");
        Arrays.fill(a,2,6,0);                         //对数组进行填充
        System.out.println(Arrays.toString(a));
    }

    //自写一个打印数组
    static void printArray(int[] a)
    {
        for (int i = 0; i < a.length; i++)
        {
            if (i==0)
            {
                System.out.print("[");
            }
            if (i == a.length-1) {
                System.out.print(a[i]+"]");
            }else{
                System.out.print(a[i]+", ");
            }
        }
    }
}