public class Demo {
    public static void main(String[] args) {
        //稀疏数组
        //定义一个数组 0：没棋子  1：黑棋  2：白棋

        int[][] array = new int[11][11];    //11*11棋盘
        array[1][2] = 1;
        array[2][3] = 2;
        //输出原始数组

        for (int[] ints : array) {
            for (int anInt : ints) {
                System.out.print(anInt+"\t");
            }
            System.out.println();
        }
        System.out.println("======================");
        //转换为稀疏数组
        //获取有效值个数
        int a = 0;
        for (int i = 0; i < 11; i++) {
            for (int j = 0; j < 11; j++) {
                if (array[i][j] != 0){
                    a++;
                }
            }
        }
        System.out.println("有效值的个数是"+a);

        //创建一个稀疏数组
        int[][] array1 = new int[a+1][3];
        array1[0][0] = 11;
        array1[0][1] = 11;
        array1[0][2] = a;

        //遍历二维数组，将非0的值，存到稀疏数组中
        int count = 0 ;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                if (array[i][j] != 0){
                    count++;
                    array1[count][0] = i;
                    array1[count][1] = j;
                    array1[count][2] = array[i][j];
                }
            }
        }
        //打印稀疏数组
        System.out.println("==================\n稀疏数组");
        for (int i = 0; i < array1.length; i++) {
            System.out.println(array1[i][0]+"\t"
                    +array1[i][1]+"\t"
                    +array1[i][2]+"\t");
        }
        System.out.println("==================\n稀疏数组还原");
        //读取稀疏数组
        int[][] array2 = new int[array1[0][0]][array1[0][1]];

        //给元素还原值
        for (int i = 1; i < array1.length; i++) {
            array2[array1[i][0]][array1[i][1]] = array1[i][2];
        }

        //打印还原
        for (int[] ints : array2) {
            for (int anInt : ints) {
                System.out.print(anInt+"\t");
            }
            System.out.println();
        }
    }
}